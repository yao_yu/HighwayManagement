#公路养护管理资料集
## 养护分类
公路养护的分类各国并不一致。按养护作业范围和工作量划分，中国把公路养护分为小修保养、中修、大修和改善四类；苏联分为保养、小修、中修和大修四类；日本分为保养和维修两大类，维修中包括更新改善的内容；英、美等国则分为具体养护和交通服务两类（不包括改善工程）。国际道路会议常设协会于1983年建议，公路养护统一划分为日常养护、定期养护、特别养护和改善工程四类。

###日常养护

对公路各组成部分（包括附属设施）每年按需要进行频繁的日常作业，其目的是保持公路原有良好状态和服务水平。日常养护的作业项目主要有：路面及其他部分的清扫；轻微损坏的修补和设施的零星更换；割草和树枝修剪；冬季除雪除冰；以及为恢复偶尔中断的交通进行紧急处理。

###定期养护

在公路使用期限内所进行的、可编制程序的、较大的养护作业。定期养护作业主要项目有：辅助设施的改进;路面磨耗层的更新或修复;路面标线、涵洞及附属设施的修复；金属桥的重新油漆等。
###特别养护

把严重恶化的路况改善到原有状态的作业。特别养护作业项目有：加强和改建已破损的路面结构；修复已破坏的路基和涵洞；防治外部因素对公路的损害，如稳定边坡、防治坍方、添建挡土墙、改善排水设施、防治水毁、预防雪崩、砍伐树木等。
###改善工程

对公路在新建或改建时遗留下的缺陷进行的改善作业。改善工程项目主要有：改善卡脖子路段，提高通行能力；校正路拱和超高，改善行车视距；调整交叉道和进入口，消除事故多发点，以策安全；采取防噪声措施；扩建和改善建筑物和其他设施；添建路旁休息区，以提高公路服务水平等。
